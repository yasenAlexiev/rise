﻿using OOPNatureReserveSimulationSolution.Food;

namespace OOPNatureReserveSimulationSolution.Animals
{
    public class Animal
    {
        private int _maxValue;
        public int Energy { get; set; }

        private HashSet<Enum> _diet;
        public int LifeSpan { get; set; }

        public Animal(int maxValue, HashSet<Enum> diet) 
        {
            _maxValue = maxValue;
            Energy = maxValue;
            _diet = diet;
            LifeSpan = 0;
        }

        public void Feed(FoodEnum food) 
        {
            if(_diet.Contains(food))
            {
                if(Energy < _maxValue)
                {
                    Energy++;
                }
            }
            else
            {
                Energy--;
            }
        }




    }
}
