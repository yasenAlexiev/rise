﻿using OOPNatureReserveSimulationSolution.Food;
using System.Collections.Generic;

namespace OOPNatureReserveSimulationSolution.Animals
{
    public class Wolf : Animal
    {
        public Wolf(int maxValue) : base(maxValue, new HashSet<Enum> { FoodEnum.Meat, FoodEnum.Granules })
        {
            Energy = maxValue;
        }


    }
}
