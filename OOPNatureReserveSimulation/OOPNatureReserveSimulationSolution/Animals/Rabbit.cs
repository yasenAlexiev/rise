﻿using OOPNatureReserveSimulationSolution.Food;

namespace OOPNatureReserveSimulationSolution.Animals
{
    public class Rabbit : Animal
    {
        public Rabbit(int maxValue) : base(maxValue, new HashSet<Enum> { FoodEnum.Grass, FoodEnum.Corn, FoodEnum.Leaf, FoodEnum.Barley, FoodEnum.Granules })
        {
            Energy = maxValue;
        }
    }
}
