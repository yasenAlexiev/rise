﻿using OOPNatureReserveSimulationSolution.Animals;
using OOPNatureReserveSimulationSolution.Food;

namespace OOPNatureReserveSimulationSolution
{
    internal class Controller
    {
        public static void SimulateLifespan()
        {
            List<Animal> animals = new List<Animal>()
            {
                new Rabbit(8), new Rabbit(8), new Rabbit(8), new Rabbit(8),
                new Wolf(10), new Wolf(10), new Wolf(10), new Wolf(10),
                new Chicken(6), new Chicken(6), new Chicken(6), new Chicken(6)
            };

            Random random = new Random();
            bool flag = true;
            while (flag)
            {
                flag = false;
                foreach (Animal animal in animals)
                {
                    if(animal.Energy > 0)
                    {
                        flag = true;
                        int foodCount = Enum.GetValues(typeof(FoodEnum)).Length;
                        animal.Feed((FoodEnum)random.Next(0, foodCount));
                        animal.LifeSpan++;
                    }
                }
            }
            Statistic(animals);
        }

        public static void Statistic(List<Animal> animals)
        {
            int minLifespan = animals.Min(a => a.LifeSpan);
            int maxLifespan = animals.Max(a => a.LifeSpan);
            double averageLifespan = animals.Average(a => a.LifeSpan);
        
            Console.WriteLine("Min " + minLifespan);
            Console.WriteLine("Max " + maxLifespan);
            Console.WriteLine("Av " + averageLifespan / animals.Count);
        }
    }
}
