﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rise;
using System;

namespace TestTasks
{
    [TestClass]
    public class TestNumbers
    {
        [TestMethod]
        public void TestCheckMaxTrue()
        {
            int overFlowInt = int.MaxValue;
            overFlowInt++;

            int actual = Numbers.CheckMax();

            Assert.AreEqual(overFlowInt, actual);

        }
        [TestMethod]
        public void TestCheckMaxFalse()
        {
            int maxValue = int.MaxValue;

            int actual = Numbers.CheckMax();

            Assert.AreNotEqual(maxValue, actual);

        }

        [TestMethod]
        public void TestIsOddTrue()
        {
            int number = 13;

            bool actural = Numbers.IsOdd(number);

            Assert.IsTrue(actural);
        }

        [TestMethod]
        public void TestIsOddFalse()
        {
            int number = 10;

            bool actural = Numbers.IsOdd(number);

            Assert.IsFalse(actural);
        }

        [TestMethod]

        public void TestIsPrimeTrue()
        {
            int number = 13;

            bool actual = Numbers.IsPrime(number);

            Assert.IsTrue(actual);
        }

        [TestMethod]

        public void TestIsPrimeFalse()
        {
            int number = 10;

            bool actual = Numbers.IsPrime(number);

            Assert.IsFalse(actual);
        }

        [TestMethod]

        public void TestIsPrimeNumberZero()
        {
            int number = 0;

            bool actual = Numbers.IsPrime(number);

            Assert.IsFalse(actual);
        }

        [TestMethod]

        public void TestMin()
        {
            int[] numbers = { 34, 54, 12, 4, 67, 3, 8 };

            int expected = 3;

            int actual = Numbers.Min(numbers);

            Assert.AreEqual(expected, actual);

        }

        [TestMethod]

        public void TestMinEmpty()
        {
            int[] arrays = { };
         
            Assert.ThrowsException<IndexOutOfRangeException>(() => Numbers.Min(arrays), "Empty");
        }

        [TestMethod]

        public void TestKthMin()
        {
            int[] numbers = { 34, 54, 12, 4, 67, 3, 8 };

            int expected = 8;

            int actual = Numbers.KthMin(3 ,numbers);

            Assert.AreEqual(expected, actual);

        }
    }

}
