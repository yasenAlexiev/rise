﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rise_tasks
{
    public class Strings
    {
        public static string ReverseString(string s)
        {
            string reverseS = default;

            for (int i = s.Length - 1; i >= 0; i--)
            {
                reverseS += s[i];
            }

            return reverseS;
        }

        public static String ReverseEveryWord(String arg)
        {
            string[] array = arg.Split(' ');

            String result = default;
            for (int i = 0; i < array.Length; i++)
            {
                char[] word = array[i].ToCharArray();

                for (int j = word.Length - 1; j >= 0; j--)
                {
                    result += word[j];
                }

                result += ' ';

            }

            result = result.Trim();


            return result;
        }

        public static bool IsPalindrome(string argument)
        {
            string reverse = default;
            for (int i = argument.Length - 1; i >= 0; i--)
            {
                reverse += argument[i];
            }

            return argument.Equals(reverse);

        }


        public static bool IsPalindrome(int number)
        {

            string reverse = default;
            string numberString = number.ToString();
            for (int i = numberString.Length - 1; i >= 0; i--)
            {
                reverse += numberString[i];
            }

            int.TryParse(reverse, out int reverseNumber);

            return number == reverseNumber;

        }
    }
}
