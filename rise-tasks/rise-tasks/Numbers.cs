﻿using System;
using System.Runtime.Remoting.Messaging;

namespace Rise
{
    public class Numbers
    {
        public static int CheckMax()
        {
            int number = int.MaxValue;
            return number + 1;
        }

        public static bool IsOdd(int value)
        {
            return (value % 2 == 1);
        }

        public static bool IsPrime(int number)
        {
            if (number <= 1) return false;
            if (number == 2) return true;
            if (number % 2 == 0) return false;

            var boundary = (int)Math.Floor(Math.Sqrt(number));

            for (int i = 3; i <= boundary; i += 2)
                if (number % i == 0)
                    return false;

            return true;
        }

        public static int Min(int[] array)
        {
            try
            {
                Array.Sort(array);

                return array[0];
            }
            catch (IndexOutOfRangeException)
            {
                throw new IndexOutOfRangeException("Empty");
            }

        }

        public static int KthMin(int k, int[] array)
        {
            Array.Sort(array);
            return array[k-1];
        }

        public static int GetOddOccurrence(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                int counter = 1;
                for (int j = 0; j < array.Length - 1; j++)
                {
                    if (array[j] == array[i])
                    {
                        counter++;
                    }
                }
                if(counter % 2 == 1)
                {
                    return array[i];
                }
            }
            return 0;
        }

        public static int GetAverage(int[] array)
        {
            int sum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                sum += array[i];
            }
            int average = sum / array.Length;

            return average;
        }

        public static long Pow(int a, int b)
        {
            long result = a;
            for (int i = 0; i < b; i++)
            {
                result *= a;
            }

            return result;
        }

        public static long GetSmallestMultiple(int N)
        {
            long result = 0;
            long number = 1;
            if(N <= 1)
            {
                return 0;
            }
            while(true)
            {
                result = number;
                for (int j = 1; j < N; j++)
                {
                    if(!(number % j == 0))
                    {
                        result = 0;
                        break;
                    }
                    
                }
                if(result != 0)
                    return result;
                number++;
            }
        }

        public static long DoubleFac(int n)
        {
            long fac = n;
            long counter = n-1;
            for (int i = 0; i < 2; i++)
            {
                for (; counter > 0; counter--)
                {
                    fac *= counter;
                }
                counter = fac - 1;
            }
            return fac;
        }

        public static long KthFAc(int k,int n)
        {
            long fac = n;
            long counter = n - 1;
            for (int i = 0; i < k; i++)
            {
                for (; counter > 0; counter--)
                {
                    fac *= counter;
                }
                counter = fac - 1;
            }
            return fac;
        }

        public static long MaximalScalarSum(int[] a, int[] b)
        {
            Array.Sort(a);
            Array.Sort(b);
            long sum = 0;
            for (int i = 0; i < a.Length; i++)
            {
                sum += a[i] * b[i];
            }
            return sum;
        }

        public static int MaxSpan(int[] numbers)
        {
            int span = 0;
            bool checkEnd = false;
            int biggestSpan = 0;
            int j = 1;
            for (int i = 0; i < numbers.Length; i++)
            {
                for (; j + i <= numbers.Length - 1; j++)
                {
                    span++;
                    if (numbers[i] == numbers[i + j])
                    {
                        if(span + 1 > biggestSpan)
                        {
                            biggestSpan = span + 1;
                        }
                        i--;
                        j++;
                        checkEnd = true;
                        break;
                    }
                }
                if (!checkEnd)
                {
                    span = 0;
                    j = 1;
                }
                checkEnd = false;
            }
            return biggestSpan;
        }

        public static bool EqualSumSides(int[] numbers)
        {
            for (int i = 2; i < numbers.Length - 2; i++)
            {


                int leftSum = 0, rightSum = 0;
                for (int j = 0; j < numbers.Length; j++)
                {
                    if (j < i)
                    {
                        leftSum += numbers[j];
                    }
                    else if (j > i)
                    {
                        rightSum += numbers[j];
                    }
                }
                if (leftSum == rightSum)
                    return true;
            }
            return false;
        }
    }
}